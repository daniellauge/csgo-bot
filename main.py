import pyautogui
from PIL import Image
import time

# Locate teamspeak icon and double-clicking
time.sleep(1)
ts_position = pyautogui.locateOnScreen("ts_location.png")
pyautogui.moveTo(ts_position)
pyautogui.click(clicks=2)

# Wait for 1 second
time.sleep(3)

# Press control and s key at the same time
pyautogui.hotkey("ctrl", "s")

# Wait for 1 second
time.sleep(1)

# Locate the connect-button and press it
ts_connect_click = pyautogui.locateOnScreen("ts_connect.png", confidence=0.9)
pyautogui.moveTo(ts_connect_click)
pyautogui.click()

# Wait for 4 seconds
time.sleep(4)

# Locate the ok-button and pres it
ts_ok_press = pyautogui.locateOnScreen("ts_ok_click.png")
pyautogui.moveTo(ts_ok_press)
pyautogui.click()

# Wait for 2 seconds
time.sleep(2)


# Locate the five player lobby and double-clicking it
ts_five_players_lobby_click = pyautogui.locateOnScreen("ts_fiveplayers_click.png")
pyautogui.moveTo(ts_five_players_lobby_click)
pyautogui.click(clicks=2)


# Locate the faceit-button and double-clicking it
faceit_click = pyautogui.locateOnScreen("faceit_click.png")
pyautogui.moveTo(faceit_click)
pyautogui.click(clicks=2)

# Wait 5 seconds
time.sleep(5)

# Locate and move to google chrome icon
google_chrome_click = pyautogui.locateOnScreen("googlechrome_click.png")
pyautogui.moveTo(google_chrome_click)
pyautogui.click(clicks=2)

# Wait 4 seconds
time.sleep(2)

# Write out faceit and search
pyautogui.typewrite("faceit.com")
pyautogui.hotkey("enter")

# Wait 4 seconds
time.sleep(4)

# Locate minimize and click
minimize_click = pyautogui.locateOnScreen("minimize_click.png")
pyautogui.moveTo(minimize_click)
pyautogui.click()


# Locate csgo icon and move to it
csgo_click = pyautogui.locateOnScreen("csgo_click.png", confidence=0.9)
pyautogui.moveTo(csgo_click)
pyautogui.click(clicks=2)
